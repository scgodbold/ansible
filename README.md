# ansible

I use this to manage my local infra.

## Setup

```bash
python -m venv venv
source venv/bin/activate
pip install -m requirements.txt
ansible-galaxy install -r requirements.yml
```
