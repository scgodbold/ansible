---

- name: Create node_exporter user
  user:
    name: node_exporter
    shell: /sbin/nologin

- name: Check if node_exporter is installed
  stat:
    path: /usr/sbin/node_exporter
  register: node_exporter

- name: Fetch node_exporter
  get_url:
    url: "https://github.com/prometheus/node_exporter/releases/download/v{{node_exporter_version}}/node_exporter-{{node_exporter_version}}.linux-amd64.tar.gz"
    dest: "/tmp/node_exporter-{{node_exporter_version}}.tar.gz"
  when: not node_exporter.stat.exists

- name: Unpack node_exporter
  unarchive:
    src: "/tmp/node_exporter-{{node_exporter_version}}.tar.gz"
    dest: /tmp
    remote_src: true
  when: not node_exporter.stat.exists

- name: Propegate node_exporter
  copy:
    src: "/tmp/node_exporter-{{node_exporter_version}}.linux-amd64/node_exporter"
    dest: /usr/sbin/node_exporter
    owner: root
    group: root
    mode: 0755
    remote_src: true
  notify:
    - restart node_exporter
  when: not node_exporter.stat.exists

- name: Cleanup node_exporter tmp dir
  file:
    state: absent
    path: "/tmp/node_exporter-{{node_exporter_version}}.linux-amd64"
  when: not node_exporter.stat.exists

- name: Cleanup node_exporter tarball
  file:
    state: absent
    path: "/tmp/node_exporter-{{node_exporter_version}}.tar.gz"
  when: not node_exporter.stat.exists

- name: Create sysconfig if missing
  file:
    path: /etc/sysconfig
    state: directory
    owner: root
    group: root

- name: Create textfile directory
  file:
    path: /var/lib/node_exporter/textfile_collector
    owner: node_exporter
    group: node_exporter
    state: directory
    recurse: true

- name: Copy node_exporter sysconfig
  copy:
    src: files/sysconfig.node_exporter
    dest: /etc/sysconfig/node_exporter
    owner: root
    group: root
  notify:
    - restart node_exporter

- name: Copy node_exporter socket config
  copy:
    src: files/node_exporter.socket
    dest: /etc/systemd/system
    owner: root
    group: root
  notify:
    - reload systemd

- name: Copy node_exporter service config
  copy:
    src: files/node_exporter.service
    dest: /etc/systemd/system
    owner: root
    group: root
  notify:
    - reload systemd
