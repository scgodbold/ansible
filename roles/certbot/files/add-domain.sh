#!/bin/bash
set -eo pipefail
domain=${1}

if [ -z "${domain}" ]; then
    echo "Must specify a domain to add"
    exit 1
fi

certbot certonly \
    --dns-digitalocean \
    --dns-digitalocean-credentials /etc/certbot/digitalocean.ini \
    -n \
    --agree-tos \
    -m admin@scgodbold.com \
    -d "${domain}"
