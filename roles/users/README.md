# Users Role

Primarily used for making actual user accounts on the system. Accounts for
services will be handled on a per need basis

## Usage

Add the new user to the defaults list an example commented is shown here

```yaml
users:
    - name: foobar                  # Required
      comment: Sir Foo Bar          # Required
      groups:                       # List of groups to be appened to the user
        - baz                       # Defaults to none if left empty
        - admin
        - docker
      state: present                # Specify if they user should be gone, defaults to present
      shell: /bin/bash              # Specify a shell, defaults to bash
      ssh_keys:
        enabled:
            - ssh-rsa ....          # Keys to be added as authorized keys to the user
        revoked:
            - ssh-rsa ....          # Keys to be removed from the user
```
